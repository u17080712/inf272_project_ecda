//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECDA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Job_Provider
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Job_Provider()
        {
            this.Available_Job = new HashSet<Available_Job>();
        }
    
        public int Job_Provider_ID { get; set; }
        public Nullable<int> User_ID { get; set; }
        public Nullable<int> Business_ID { get; set; }
        public Nullable<int> Individual_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Available_Job> Available_Job { get; set; }
        public virtual Business Business { get; set; }
        public virtual Individual Individual { get; set; }
        public virtual user user { get; set; }
    }
}
