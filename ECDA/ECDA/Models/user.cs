//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECDA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user()
        {
            this.Career_Advisor = new HashSet<Career_Advisor>();
            this.Donation_Business = new HashSet<Donation_Business>();
            this.Job_Provider = new HashSet<Job_Provider>();
            this.Job_Seeker = new HashSet<Job_Seeker>();
        }
    
        public int User_ID { get; set; }
        public string User_Number { get; set; }
        public string User_Name { get; set; }
        public string UserPassword { get; set; }
        public Nullable<System.DateTime> UserPasswordCreatedDate { get; set; }
        public Nullable<System.DateTime> UserPasswordUpdatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Career_Advisor> Career_Advisor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Donation_Business> Donation_Business { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job_Provider> Job_Provider { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Job_Seeker> Job_Seeker { get; set; }
    }
}
