//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECDA.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Consultation_Hour
    {
        public int ConsultationHour_ID { get; set; }
        public Nullable<int> Career_Advisor_ID { get; set; }
        public string Day { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
    
        public virtual Career_Advisor Career_Advisor { get; set; }
    }
}
