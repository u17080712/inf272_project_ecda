﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ECDA.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ECDA_DatabaseEntities : DbContext
    {
        public ECDA_DatabaseEntities()
            : base("name=ECDA_DatabaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Available_Job> Available_Job { get; set; }
        public virtual DbSet<Business> Businesses { get; set; }
        public virtual DbSet<Career_Advisor> Career_Advisor { get; set; }
        public virtual DbSet<Consultation_Booking> Consultation_Booking { get; set; }
        public virtual DbSet<Consultation_Hour> Consultation_Hour { get; set; }
        public virtual DbSet<Donation> Donations { get; set; }
        public virtual DbSet<Donation_Business> Donation_Business { get; set; }
        public virtual DbSet<Individual> Individuals { get; set; }
        public virtual DbSet<Job_Application> Job_Application { get; set; }
        public virtual DbSet<Job_Provider> Job_Provider { get; set; }
        public virtual DbSet<Job_Seeker> Job_Seeker { get; set; }
        public virtual DbSet<Job_Seeker_CV> Job_Seeker_CV { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<user> users { get; set; }
    }
}
