﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Job_ProviderController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Job_Provider
        public ActionResult Index()
        {
            var job_Provider = db.Job_Provider.Include(j => j.Business).Include(j => j.Individual).Include(j => j.user);
            return View(job_Provider.ToList());
        }

        // GET: Job_Provider/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Provider job_Provider = db.Job_Provider.Find(id);
            if (job_Provider == null)
            {
                return HttpNotFound();
            }
            return View(job_Provider);
        }

        // GET: Job_Provider/Create
        public ActionResult Create()
        {
            ViewBag.Business_ID = new SelectList(db.Businesses, "Business_ID", "BusinessName");
            ViewBag.Individual_ID = new SelectList(db.Individuals, "Individual_ID", "IndividualName");
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number");
            return View();
        }

        // POST: Job_Provider/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Job_Provider_ID,User_ID,Business_ID,Individual_ID")] Job_Provider job_Provider)
        {
            if (ModelState.IsValid)
            {
                db.Job_Provider.Add(job_Provider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Business_ID = new SelectList(db.Businesses, "Business_ID", "BusinessName", job_Provider.Business_ID);
            ViewBag.Individual_ID = new SelectList(db.Individuals, "Individual_ID", "IndividualName", job_Provider.Individual_ID);
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Provider.User_ID);
            return View(job_Provider);
        }

        // GET: Job_Provider/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Provider job_Provider = db.Job_Provider.Find(id);
            if (job_Provider == null)
            {
                return HttpNotFound();
            }
            ViewBag.Business_ID = new SelectList(db.Businesses, "Business_ID", "BusinessName", job_Provider.Business_ID);
            ViewBag.Individual_ID = new SelectList(db.Individuals, "Individual_ID", "IndividualName", job_Provider.Individual_ID);
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Provider.User_ID);
            return View(job_Provider);
        }

        // POST: Job_Provider/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Job_Provider_ID,User_ID,Business_ID,Individual_ID")] Job_Provider job_Provider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Provider).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Business_ID = new SelectList(db.Businesses, "Business_ID", "BusinessName", job_Provider.Business_ID);
            ViewBag.Individual_ID = new SelectList(db.Individuals, "Individual_ID", "IndividualName", job_Provider.Individual_ID);
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Provider.User_ID);
            return View(job_Provider);
        }

        // GET: Job_Provider/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Provider job_Provider = db.Job_Provider.Find(id);
            if (job_Provider == null)
            {
                return HttpNotFound();
            }
            return View(job_Provider);
        }

        // POST: Job_Provider/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job_Provider job_Provider = db.Job_Provider.Find(id);
            db.Job_Provider.Remove(job_Provider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
