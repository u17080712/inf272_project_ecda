﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Donation_BusinessController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Donation_Business
        public ActionResult Index()
        {
            var donation_Business = db.Donation_Business.Include(d => d.user);
            return View(donation_Business.ToList());
        }

        // GET: Donation_Business/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donation_Business donation_Business = db.Donation_Business.Find(id);
            if (donation_Business == null)
            {
                return HttpNotFound();
            }
            return View(donation_Business);
        }

        // GET: Donation_Business/Create
        public ActionResult Create()
        {
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number");
            return View();
        }

        // POST: Donation_Business/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Donation_Business_ID,User_ID,BusinessName,WebsiteURL,CardHolderName,CardNumber,CVV,Bank,BranchCode")] Donation_Business donation_Business)
        {
            if (ModelState.IsValid)
            {
                db.Donation_Business.Add(donation_Business);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", donation_Business.User_ID);
            return View(donation_Business);
        }

        // GET: Donation_Business/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donation_Business donation_Business = db.Donation_Business.Find(id);
            if (donation_Business == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", donation_Business.User_ID);
            return View(donation_Business);
        }

        // POST: Donation_Business/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Donation_Business_ID,User_ID,BusinessName,WebsiteURL,CardHolderName,CardNumber,CVV,Bank,BranchCode")] Donation_Business donation_Business)
        {
            if (ModelState.IsValid)
            {
                db.Entry(donation_Business).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", donation_Business.User_ID);
            return View(donation_Business);
        }

        // GET: Donation_Business/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donation_Business donation_Business = db.Donation_Business.Find(id);
            if (donation_Business == null)
            {
                return HttpNotFound();
            }
            return View(donation_Business);
        }

        // POST: Donation_Business/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Donation_Business donation_Business = db.Donation_Business.Find(id);
            db.Donation_Business.Remove(donation_Business);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
