﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Consultation_BookingController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Consultation_Booking
        public ActionResult Index()
        {
            var consultation_Booking = db.Consultation_Booking.Include(c => c.Career_Advisor).Include(c => c.Job_Seeker);
            return View(consultation_Booking.ToList());
        }

        // GET: Consultation_Booking/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Booking consultation_Booking = db.Consultation_Booking.Find(id);
            if (consultation_Booking == null)
            {
                return HttpNotFound();
            }
            return View(consultation_Booking);
        }

        // GET: Consultation_Booking/Create
        public ActionResult Create()
        {
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name");
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name");
            return View();
        }

        // POST: Consultation_Booking/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Consultation_Booking_ID,Job_Seeker_ID,Career_Advisor_ID,BookingStatus,BookingDate,ConsultationLink")] Consultation_Booking consultation_Booking)
        {
            if (ModelState.IsValid)
            {
                db.Consultation_Booking.Add(consultation_Booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Booking.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", consultation_Booking.Job_Seeker_ID);
            return View(consultation_Booking);
        }

        // GET: Consultation_Booking/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Booking consultation_Booking = db.Consultation_Booking.Find(id);
            if (consultation_Booking == null)
            {
                return HttpNotFound();
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Booking.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", consultation_Booking.Job_Seeker_ID);
            return View(consultation_Booking);
        }

        // POST: Consultation_Booking/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Consultation_Booking_ID,Job_Seeker_ID,Career_Advisor_ID,BookingStatus,BookingDate,ConsultationLink")] Consultation_Booking consultation_Booking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consultation_Booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Booking.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", consultation_Booking.Job_Seeker_ID);
            return View(consultation_Booking);
        }

        // GET: Consultation_Booking/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Booking consultation_Booking = db.Consultation_Booking.Find(id);
            if (consultation_Booking == null)
            {
                return HttpNotFound();
            }
            return View(consultation_Booking);
        }

        // POST: Consultation_Booking/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consultation_Booking consultation_Booking = db.Consultation_Booking.Find(id);
            db.Consultation_Booking.Remove(consultation_Booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
