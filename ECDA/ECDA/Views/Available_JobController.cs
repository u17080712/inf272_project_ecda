﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Available_JobController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Available_Job
        public ActionResult Index()
        {
            var available_Job = db.Available_Job.Include(a => a.Job_Provider);
            return View(available_Job.ToList());
        }

        // GET: Available_Job/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Available_Job available_Job = db.Available_Job.Find(id);
            if (available_Job == null)
            {
                return HttpNotFound();
            }
            return View(available_Job);
        }

        // GET: Available_Job/Create
        public ActionResult Create()
        {
            ViewBag.Job_Provider_ID = new SelectList(db.Job_Provider, "Job_Provider_ID", "Job_Provider_ID");
            return View();
        }

        // POST: Available_Job/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Available_Job_ID,Job_Provider_ID,Title,Position,Type,ApplicationDueDate,Payment,PaymentNegotiable,StreetAddress,AdressLineTwo,City_Town,Province,Country,Zip_PostalCode,Description,Requirements")] Available_Job available_Job)
        {
            if (ModelState.IsValid)
            {
                db.Available_Job.Add(available_Job);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Job_Provider_ID = new SelectList(db.Job_Provider, "Job_Provider_ID", "Job_Provider_ID", available_Job.Job_Provider_ID);
            return View(available_Job);
        }

        // GET: Available_Job/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Available_Job available_Job = db.Available_Job.Find(id);
            if (available_Job == null)
            {
                return HttpNotFound();
            }
            ViewBag.Job_Provider_ID = new SelectList(db.Job_Provider, "Job_Provider_ID", "Job_Provider_ID", available_Job.Job_Provider_ID);
            return View(available_Job);
        }

        // POST: Available_Job/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Available_Job_ID,Job_Provider_ID,Title,Position,Type,ApplicationDueDate,Payment,PaymentNegotiable,StreetAddress,AdressLineTwo,City_Town,Province,Country,Zip_PostalCode,Description,Requirements")] Available_Job available_Job)
        {
            if (ModelState.IsValid)
            {
                db.Entry(available_Job).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Job_Provider_ID = new SelectList(db.Job_Provider, "Job_Provider_ID", "Job_Provider_ID", available_Job.Job_Provider_ID);
            return View(available_Job);
        }

        // GET: Available_Job/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Available_Job available_Job = db.Available_Job.Find(id);
            if (available_Job == null)
            {
                return HttpNotFound();
            }
            return View(available_Job);
        }

        // POST: Available_Job/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Available_Job available_Job = db.Available_Job.Find(id);
            db.Available_Job.Remove(available_Job);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
