﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Job_SeekerController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Job_Seeker
        public ActionResult Index()
        {
            var job_Seeker = db.Job_Seeker.Include(j => j.user);
            return View(job_Seeker.ToList());
        }

        // GET: Job_Seeker/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker job_Seeker = db.Job_Seeker.Find(id);
            if (job_Seeker == null)
            {
                return HttpNotFound();
            }
            return View(job_Seeker);
        }

        // GET: Job_Seeker/Create
        public ActionResult Create()
        {
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number");
            return View();
        }

        // POST: Job_Seeker/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Job_Seeker_ID,User_ID,Job_Seeker_Name,Job_Seeker_Surname,Job_Seeker_ID_Number,Job_Seeker_Email,Job_Seeker_ContactNumber,Age,BirthDate")] Job_Seeker job_Seeker)
        {
            if (ModelState.IsValid)
            {
                db.Job_Seeker.Add(job_Seeker);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Seeker.User_ID);
            return View(job_Seeker);
        }

        // GET: Job_Seeker/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker job_Seeker = db.Job_Seeker.Find(id);
            if (job_Seeker == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Seeker.User_ID);
            return View(job_Seeker);
        }

        // POST: Job_Seeker/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Job_Seeker_ID,User_ID,Job_Seeker_Name,Job_Seeker_Surname,Job_Seeker_ID_Number,Job_Seeker_Email,Job_Seeker_ContactNumber,Age,BirthDate")] Job_Seeker job_Seeker)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Seeker).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", job_Seeker.User_ID);
            return View(job_Seeker);
        }

        // GET: Job_Seeker/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker job_Seeker = db.Job_Seeker.Find(id);
            if (job_Seeker == null)
            {
                return HttpNotFound();
            }
            return View(job_Seeker);
        }

        // POST: Job_Seeker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job_Seeker job_Seeker = db.Job_Seeker.Find(id);
            db.Job_Seeker.Remove(job_Seeker);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
