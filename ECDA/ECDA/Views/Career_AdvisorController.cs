﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Career_AdvisorController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Career_Advisor
        public ActionResult Index()
        {
            var career_Advisor = db.Career_Advisor.Include(c => c.user);
            return View(career_Advisor.ToList());
        }

        // GET: Career_Advisor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Career_Advisor career_Advisor = db.Career_Advisor.Find(id);
            if (career_Advisor == null)
            {
                return HttpNotFound();
            }
            return View(career_Advisor);
        }

        // GET: Career_Advisor/Create
        public ActionResult Create()
        {
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number");
            return View();
        }

        // POST: Career_Advisor/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Career_Advisor_ID,User_ID,Career_Advisor_Name,Career_Advisor_Email_Address,Career_Advisor_ContactNumber,Career_Advice_Experience,About")] Career_Advisor career_Advisor)
        {
            if (ModelState.IsValid)
            {
                db.Career_Advisor.Add(career_Advisor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", career_Advisor.User_ID);
            return View(career_Advisor);
        }

        // GET: Career_Advisor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Career_Advisor career_Advisor = db.Career_Advisor.Find(id);
            if (career_Advisor == null)
            {
                return HttpNotFound();
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", career_Advisor.User_ID);
            return View(career_Advisor);
        }

        // POST: Career_Advisor/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Career_Advisor_ID,User_ID,Career_Advisor_Name,Career_Advisor_Email_Address,Career_Advisor_ContactNumber,Career_Advice_Experience,About")] Career_Advisor career_Advisor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(career_Advisor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.User_ID = new SelectList(db.users, "User_ID", "User_Number", career_Advisor.User_ID);
            return View(career_Advisor);
        }

        // GET: Career_Advisor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Career_Advisor career_Advisor = db.Career_Advisor.Find(id);
            if (career_Advisor == null)
            {
                return HttpNotFound();
            }
            return View(career_Advisor);
        }

        // POST: Career_Advisor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Career_Advisor career_Advisor = db.Career_Advisor.Find(id);
            db.Career_Advisor.Remove(career_Advisor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
