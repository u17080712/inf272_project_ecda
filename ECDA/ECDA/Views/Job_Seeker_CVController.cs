﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Job_Seeker_CVController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Job_Seeker_CV
        public ActionResult Index()
        {
            var job_Seeker_CV = db.Job_Seeker_CV.Include(j => j.Job_Seeker);
            return View(job_Seeker_CV.ToList());
        }

        // GET: Job_Seeker_CV/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker_CV job_Seeker_CV = db.Job_Seeker_CV.Find(id);
            if (job_Seeker_CV == null)
            {
                return HttpNotFound();
            }
            return View(job_Seeker_CV);
        }

        // GET: Job_Seeker_CV/Create
        public ActionResult Create()
        {
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name");
            return View();
        }

        // POST: Job_Seeker_CV/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Job_Seeker_CV_ID,Job_Seeker_ID,CV_FileName")] Job_Seeker_CV job_Seeker_CV)
        {
            if (ModelState.IsValid)
            {
                db.Job_Seeker_CV.Add(job_Seeker_CV);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Seeker_CV.Job_Seeker_ID);
            return View(job_Seeker_CV);
        }

        // GET: Job_Seeker_CV/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker_CV job_Seeker_CV = db.Job_Seeker_CV.Find(id);
            if (job_Seeker_CV == null)
            {
                return HttpNotFound();
            }
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Seeker_CV.Job_Seeker_ID);
            return View(job_Seeker_CV);
        }

        // POST: Job_Seeker_CV/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Job_Seeker_CV_ID,Job_Seeker_ID,CV_FileName")] Job_Seeker_CV job_Seeker_CV)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Seeker_CV).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Seeker_CV.Job_Seeker_ID);
            return View(job_Seeker_CV);
        }

        // GET: Job_Seeker_CV/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Seeker_CV job_Seeker_CV = db.Job_Seeker_CV.Find(id);
            if (job_Seeker_CV == null)
            {
                return HttpNotFound();
            }
            return View(job_Seeker_CV);
        }

        // POST: Job_Seeker_CV/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job_Seeker_CV job_Seeker_CV = db.Job_Seeker_CV.Find(id);
            db.Job_Seeker_CV.Remove(job_Seeker_CV);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
