﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Consultation_HourController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Consultation_Hour
        public ActionResult Index()
        {
            var consultation_Hour = db.Consultation_Hour.Include(c => c.Career_Advisor);
            return View(consultation_Hour.ToList());
        }

        // GET: Consultation_Hour/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Hour consultation_Hour = db.Consultation_Hour.Find(id);
            if (consultation_Hour == null)
            {
                return HttpNotFound();
            }
            return View(consultation_Hour);
        }

        // GET: Consultation_Hour/Create
        public ActionResult Create()
        {
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name");
            return View();
        }

        // POST: Consultation_Hour/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ConsultationHour_ID,Career_Advisor_ID,Day,StartTime,EndTime")] Consultation_Hour consultation_Hour)
        {
            if (ModelState.IsValid)
            {
                db.Consultation_Hour.Add(consultation_Hour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Hour.Career_Advisor_ID);
            return View(consultation_Hour);
        }

        // GET: Consultation_Hour/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Hour consultation_Hour = db.Consultation_Hour.Find(id);
            if (consultation_Hour == null)
            {
                return HttpNotFound();
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Hour.Career_Advisor_ID);
            return View(consultation_Hour);
        }

        // POST: Consultation_Hour/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ConsultationHour_ID,Career_Advisor_ID,Day,StartTime,EndTime")] Consultation_Hour consultation_Hour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consultation_Hour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", consultation_Hour.Career_Advisor_ID);
            return View(consultation_Hour);
        }

        // GET: Consultation_Hour/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consultation_Hour consultation_Hour = db.Consultation_Hour.Find(id);
            if (consultation_Hour == null)
            {
                return HttpNotFound();
            }
            return View(consultation_Hour);
        }

        // POST: Consultation_Hour/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consultation_Hour consultation_Hour = db.Consultation_Hour.Find(id);
            db.Consultation_Hour.Remove(consultation_Hour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
