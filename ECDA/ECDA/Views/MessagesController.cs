﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class MessagesController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Messages
        public ActionResult Index()
        {
            var messages = db.Messages.Include(m => m.Career_Advisor).Include(m => m.Job_Seeker);
            return View(messages.ToList());
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name");
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name");
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Message_ID,Job_Seeker_ID,Career_Advisor_ID,Message_Sent_Time,Message_Opened_Time,Message_Sent_Date,Message1")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", message.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", message.Job_Seeker_ID);
            return View(message);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", message.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", message.Job_Seeker_ID);
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Message_ID,Job_Seeker_ID,Career_Advisor_ID,Message_Sent_Time,Message_Opened_Time,Message_Sent_Date,Message1")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Career_Advisor_ID = new SelectList(db.Career_Advisor, "Career_Advisor_ID", "Career_Advisor_Name", message.Career_Advisor_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", message.Job_Seeker_ID);
            return View(message);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
