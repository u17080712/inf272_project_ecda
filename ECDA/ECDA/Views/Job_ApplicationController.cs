﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECDA.Models;

namespace ECDA.Views
{
    public class Job_ApplicationController : Controller
    {
        private ECDA_DatabaseEntities db = new ECDA_DatabaseEntities();

        // GET: Job_Application
        public ActionResult Index()
        {
            var job_Application = db.Job_Application.Include(j => j.Available_Job).Include(j => j.Job_Seeker);
            return View(job_Application.ToList());
        }

        // GET: Job_Application/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Application job_Application = db.Job_Application.Find(id);
            if (job_Application == null)
            {
                return HttpNotFound();
            }
            return View(job_Application);
        }

        // GET: Job_Application/Create
        public ActionResult Create()
        {
            ViewBag.Available_Job_ID = new SelectList(db.Available_Job, "Available_Job_ID", "Title");
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name");
            return View();
        }

        // POST: Job_Application/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationID,Job_Seeker_ID,Available_Job_ID,ApplicationDate,ApplicationStatus")] Job_Application job_Application)
        {
            if (ModelState.IsValid)
            {
                db.Job_Application.Add(job_Application);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Available_Job_ID = new SelectList(db.Available_Job, "Available_Job_ID", "Title", job_Application.Available_Job_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Application.Job_Seeker_ID);
            return View(job_Application);
        }

        // GET: Job_Application/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Application job_Application = db.Job_Application.Find(id);
            if (job_Application == null)
            {
                return HttpNotFound();
            }
            ViewBag.Available_Job_ID = new SelectList(db.Available_Job, "Available_Job_ID", "Title", job_Application.Available_Job_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Application.Job_Seeker_ID);
            return View(job_Application);
        }

        // POST: Job_Application/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationID,Job_Seeker_ID,Available_Job_ID,ApplicationDate,ApplicationStatus")] Job_Application job_Application)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job_Application).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Available_Job_ID = new SelectList(db.Available_Job, "Available_Job_ID", "Title", job_Application.Available_Job_ID);
            ViewBag.Job_Seeker_ID = new SelectList(db.Job_Seeker, "Job_Seeker_ID", "Job_Seeker_Name", job_Application.Job_Seeker_ID);
            return View(job_Application);
        }

        // GET: Job_Application/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job_Application job_Application = db.Job_Application.Find(id);
            if (job_Application == null)
            {
                return HttpNotFound();
            }
            return View(job_Application);
        }

        // POST: Job_Application/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job_Application job_Application = db.Job_Application.Find(id);
            db.Job_Application.Remove(job_Application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
