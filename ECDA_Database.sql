USE [master]
GO
/****** Object:  Database [ECDA Database]    Script Date: 11/5/2020 7:03:58 PM ******/
CREATE DATABASE [ECDA Database]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ECDA Database', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ECDA Database.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ECDA Database_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ECDA Database_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ECDA Database] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ECDA Database].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ECDA Database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ECDA Database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ECDA Database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ECDA Database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ECDA Database] SET ARITHABORT OFF 
GO
ALTER DATABASE [ECDA Database] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ECDA Database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ECDA Database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ECDA Database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ECDA Database] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ECDA Database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ECDA Database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ECDA Database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ECDA Database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ECDA Database] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ECDA Database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ECDA Database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ECDA Database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ECDA Database] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ECDA Database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ECDA Database] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ECDA Database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ECDA Database] SET RECOVERY FULL 
GO
ALTER DATABASE [ECDA Database] SET  MULTI_USER 
GO
ALTER DATABASE [ECDA Database] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ECDA Database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ECDA Database] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ECDA Database] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ECDA Database] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ECDA Database', N'ON'
GO
USE [ECDA Database]
GO
/****** Object:  Table [dbo].[Available_Job]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Available_Job](
	[Available_Job_ID] [int] NOT NULL,
	[Job_Provider_ID] [int] NULL,
	[Title] [varchar](50) NULL,
	[Position] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[ApplicationDueDate] [datetime] NULL,
	[Payment] [float] NULL,
	[PaymentNegotiable] [binary](50) NULL,
	[StreetAddress] [varchar](50) NULL,
	[AdressLineTwo] [varchar](50) NULL,
	[City_Town] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Zip_PostalCode] [varchar](4) NULL,
	[Description] [varchar](8000) NULL,
	[Requirements] [varchar](8000) NULL,
 CONSTRAINT [PK_Available_Job] PRIMARY KEY CLUSTERED 
(
	[Available_Job_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Business]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Business](
	[Business_ID] [int] NOT NULL,
	[BusinessName] [varchar](55) NULL,
	[BusinessEmailAddress] [varchar](320) NULL,
	[BusinessContactNumber] [varchar](12) NULL,
	[WebsiteURL] [varchar](58) NULL,
	[StreetAddress] [varchar](50) NULL,
	[AdressLineTwo] [varchar](50) NULL,
	[City_Town] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Zip_PostalCode] [varchar](4) NULL,
 CONSTRAINT [PK_Business] PRIMARY KEY CLUSTERED 
(
	[Business_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Career_Advisor]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Career_Advisor](
	[Career_Advisor_ID] [int] NOT NULL,
	[User_ID] [int] NULL,
	[Career_Advisor_Name] [varchar](50) NULL,
	[Career_Advisor_Email_Address] [varchar](50) NULL,
	[Career_Advisor_ContactNumber] [varchar](12) NULL,
	[Career_Advice_Experience] [varchar](8000) NULL,
	[About] [varchar](8000) NULL,
 CONSTRAINT [PK_Career_Advisor] PRIMARY KEY CLUSTERED 
(
	[Career_Advisor_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Consultation_Booking]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Consultation_Booking](
	[Consultation_Booking_ID] [int] NOT NULL,
	[Job_Seeker_ID] [int] NULL,
	[Career_Advisor_ID] [int] NULL,
	[BookingStatus] [varchar](30) NULL,
	[BookingDate] [datetime] NULL,
	[ConsultationLink] [varchar](60) NULL,
 CONSTRAINT [PK_Consultation_Booking] PRIMARY KEY CLUSTERED 
(
	[Consultation_Booking_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Consultation_Hour]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Consultation_Hour](
	[ConsultationHour_ID] [int] NOT NULL,
	[Career_Advisor_ID] [int] NULL,
	[Day] [varchar](9) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
 CONSTRAINT [PK_Consultation_Hour] PRIMARY KEY CLUSTERED 
(
	[ConsultationHour_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Donation]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Donation](
	[Donation_ID] [int] NOT NULL,
	[Donation_Business_ID] [int] NULL,
	[Date] [datetime] NULL,
	[Amount] [float] NULL,
 CONSTRAINT [PK_RealDonation] PRIMARY KEY CLUSTERED 
(
	[Donation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Donation_Business]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Donation_Business](
	[Donation_Business_ID] [int] NOT NULL,
	[User_ID] [int] NULL,
	[BusinessName] [varchar](55) NULL,
	[WebsiteURL] [varchar](58) NULL,
	[CardHolderName] [varchar](50) NULL,
	[CardNumber] [varchar](19) NULL,
	[CVV] [varchar](3) NULL,
	[Bank] [varchar](50) NULL,
	[BranchCode] [varchar](4) NULL,
 CONSTRAINT [PK_Donation_Business] PRIMARY KEY CLUSTERED 
(
	[Donation_Business_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Individual]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Individual](
	[Individual_ID] [int] NOT NULL,
	[IndividualName] [varchar](50) NULL,
	[IndividualSurname] [varchar](50) NULL,
	[IndividualEmailAddress] [varchar](320) NULL,
	[IndividualContactNumber] [varchar](12) NULL,
	[StreetAddress] [varchar](50) NULL,
	[AddressLineTwo] [varchar](50) NULL,
	[City_Town] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Zip_PostalCode] [varchar](4) NULL,
 CONSTRAINT [PK_RealIndividual] PRIMARY KEY CLUSTERED 
(
	[Individual_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Job_Application]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job_Application](
	[ApplicationID] [int] NOT NULL,
	[Job_Seeker_ID] [int] NULL,
	[Available_Job_ID] [int] NULL,
	[ApplicationDate] [datetime] NULL,
	[ApplicationStatus] [varchar](20) NULL,
 CONSTRAINT [PK_RealJob_Application] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Job_Provider]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job_Provider](
	[Job_Provider_ID] [int] NOT NULL,
	[User_ID] [int] NULL,
	[Business_ID] [int] NULL,
	[Individual_ID] [int] NULL,
 CONSTRAINT [PK_Job_Provider] PRIMARY KEY CLUSTERED 
(
	[Job_Provider_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Job_Seeker]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job_Seeker](
	[Job_Seeker_ID] [int] NOT NULL,
	[User_ID] [int] NULL,
	[Job_Seeker_Name] [varchar](50) NULL,
	[Job_Seeker_Surname] [varchar](50) NULL,
	[Job_Seeker_ID_Number] [varchar](13) NULL,
	[Job_Seeker_Email] [varchar](320) NULL,
	[Job_Seeker_ContactNumber] [varchar](12) NULL,
	[Age] [int] NULL,
	[BirthDate] [datetime] NULL,
 CONSTRAINT [PK_Job_Seeker] PRIMARY KEY CLUSTERED 
(
	[Job_Seeker_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Job_Seeker_CV]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job_Seeker_CV](
	[Job_Seeker_CV_ID] [int] NOT NULL,
	[Job_Seeker_ID] [int] NULL,
	[CV_FileName] [varchar](50) NULL,
 CONSTRAINT [PK_Job_Seeker_CV] PRIMARY KEY CLUSTERED 
(
	[Job_Seeker_CV_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Message]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Message](
	[Message_ID] [int] NOT NULL,
	[Job_Seeker_ID] [int] NULL,
	[Career_Advisor_ID] [int] NULL,
	[Message_Sent_Time] [datetime] NULL,
	[Message_Opened_Time] [datetime] NULL,
	[Message_Sent_Date] [datetime] NULL,
	[Message] [varchar](900) NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[Message_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Survey]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Survey](
	[SurveyID] [int] NOT NULL,
	[JobSeekerID] [int] NULL,
	[QuestionOneAnswer] [varchar](6) NULL,
	[QuestionTwoAnswer] [varchar](6) NULL,
	[QuestionThreeAnswer] [varchar](6) NULL,
	[QuestionFourAnswer] [varchar](6) NULL,
	[QuestionFiveAnswer] [varchar](6) NULL,
 CONSTRAINT [PK_Survey] PRIMARY KEY CLUSTERED 
(
	[SurveyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 11/5/2020 7:03:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[User_ID] [int] NOT NULL,
	[User_Number] [varchar](30) NULL,
	[User_Name] [varchar](40) NULL,
	[UserPassword] [varchar](30) NULL,
	[UserPasswordCreatedDate] [datetime] NULL,
	[UserPasswordUpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_user_1] PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Available_Job]  WITH CHECK ADD  CONSTRAINT [FK_Available_Job_Job_Provider] FOREIGN KEY([Job_Provider_ID])
REFERENCES [dbo].[Job_Provider] ([Job_Provider_ID])
GO
ALTER TABLE [dbo].[Available_Job] CHECK CONSTRAINT [FK_Available_Job_Job_Provider]
GO
ALTER TABLE [dbo].[Career_Advisor]  WITH CHECK ADD  CONSTRAINT [FK_Career_Advisor_user] FOREIGN KEY([User_ID])
REFERENCES [dbo].[user] ([User_ID])
GO
ALTER TABLE [dbo].[Career_Advisor] CHECK CONSTRAINT [FK_Career_Advisor_user]
GO
ALTER TABLE [dbo].[Consultation_Booking]  WITH CHECK ADD  CONSTRAINT [FK_Consultation_Booking_Career_Advisor] FOREIGN KEY([Career_Advisor_ID])
REFERENCES [dbo].[Career_Advisor] ([Career_Advisor_ID])
GO
ALTER TABLE [dbo].[Consultation_Booking] CHECK CONSTRAINT [FK_Consultation_Booking_Career_Advisor]
GO
ALTER TABLE [dbo].[Consultation_Booking]  WITH CHECK ADD  CONSTRAINT [FK_Consultation_Booking_Job_Seeker] FOREIGN KEY([Job_Seeker_ID])
REFERENCES [dbo].[Job_Seeker] ([Job_Seeker_ID])
GO
ALTER TABLE [dbo].[Consultation_Booking] CHECK CONSTRAINT [FK_Consultation_Booking_Job_Seeker]
GO
ALTER TABLE [dbo].[Consultation_Hour]  WITH CHECK ADD  CONSTRAINT [FK_Consultation_Hour_Career_Advisor] FOREIGN KEY([Career_Advisor_ID])
REFERENCES [dbo].[Career_Advisor] ([Career_Advisor_ID])
GO
ALTER TABLE [dbo].[Consultation_Hour] CHECK CONSTRAINT [FK_Consultation_Hour_Career_Advisor]
GO
ALTER TABLE [dbo].[Donation]  WITH CHECK ADD  CONSTRAINT [FK_Donation_Donation_Business] FOREIGN KEY([Donation_Business_ID])
REFERENCES [dbo].[Donation_Business] ([Donation_Business_ID])
GO
ALTER TABLE [dbo].[Donation] CHECK CONSTRAINT [FK_Donation_Donation_Business]
GO
ALTER TABLE [dbo].[Donation_Business]  WITH CHECK ADD  CONSTRAINT [FK_Donation_Business_user] FOREIGN KEY([User_ID])
REFERENCES [dbo].[user] ([User_ID])
GO
ALTER TABLE [dbo].[Donation_Business] CHECK CONSTRAINT [FK_Donation_Business_user]
GO
ALTER TABLE [dbo].[Job_Application]  WITH CHECK ADD  CONSTRAINT [FK_Job_Application_Available_Job] FOREIGN KEY([Available_Job_ID])
REFERENCES [dbo].[Available_Job] ([Available_Job_ID])
GO
ALTER TABLE [dbo].[Job_Application] CHECK CONSTRAINT [FK_Job_Application_Available_Job]
GO
ALTER TABLE [dbo].[Job_Application]  WITH CHECK ADD  CONSTRAINT [FK_Job_Application_Job_Seeker] FOREIGN KEY([Job_Seeker_ID])
REFERENCES [dbo].[Job_Seeker] ([Job_Seeker_ID])
GO
ALTER TABLE [dbo].[Job_Application] CHECK CONSTRAINT [FK_Job_Application_Job_Seeker]
GO
ALTER TABLE [dbo].[Job_Provider]  WITH CHECK ADD  CONSTRAINT [FK_Job_Provider_Business] FOREIGN KEY([Business_ID])
REFERENCES [dbo].[Business] ([Business_ID])
GO
ALTER TABLE [dbo].[Job_Provider] CHECK CONSTRAINT [FK_Job_Provider_Business]
GO
ALTER TABLE [dbo].[Job_Provider]  WITH CHECK ADD  CONSTRAINT [FK_Job_Provider_Individual] FOREIGN KEY([Individual_ID])
REFERENCES [dbo].[Individual] ([Individual_ID])
GO
ALTER TABLE [dbo].[Job_Provider] CHECK CONSTRAINT [FK_Job_Provider_Individual]
GO
ALTER TABLE [dbo].[Job_Provider]  WITH CHECK ADD  CONSTRAINT [FK_Job_Provider_user] FOREIGN KEY([User_ID])
REFERENCES [dbo].[user] ([User_ID])
GO
ALTER TABLE [dbo].[Job_Provider] CHECK CONSTRAINT [FK_Job_Provider_user]
GO
ALTER TABLE [dbo].[Job_Seeker]  WITH CHECK ADD  CONSTRAINT [FK_Job_Seeker_user] FOREIGN KEY([User_ID])
REFERENCES [dbo].[user] ([User_ID])
GO
ALTER TABLE [dbo].[Job_Seeker] CHECK CONSTRAINT [FK_Job_Seeker_user]
GO
ALTER TABLE [dbo].[Job_Seeker_CV]  WITH CHECK ADD  CONSTRAINT [FK_Job_Seeker_CV_Job_Seeker] FOREIGN KEY([Job_Seeker_ID])
REFERENCES [dbo].[Job_Seeker] ([Job_Seeker_ID])
GO
ALTER TABLE [dbo].[Job_Seeker_CV] CHECK CONSTRAINT [FK_Job_Seeker_CV_Job_Seeker]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Career_Advisor] FOREIGN KEY([Career_Advisor_ID])
REFERENCES [dbo].[Career_Advisor] ([Career_Advisor_ID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Career_Advisor]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_Job_Seeker] FOREIGN KEY([Job_Seeker_ID])
REFERENCES [dbo].[Job_Seeker] ([Job_Seeker_ID])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_Job_Seeker]
GO
ALTER TABLE [dbo].[Survey]  WITH CHECK ADD  CONSTRAINT [FK_Survey_Job_Seeker] FOREIGN KEY([JobSeekerID])
REFERENCES [dbo].[Job_Seeker] ([Job_Seeker_ID])
GO
ALTER TABLE [dbo].[Survey] CHECK CONSTRAINT [FK_Survey_Job_Seeker]
GO
USE [master]
GO
ALTER DATABASE [ECDA Database] SET  READ_WRITE 
GO
